# iRedmail Deployer
---
The goal of this repo is to automate the process of installing iRedmail with
the common setup/configuration we are doing when we are setting it up. 

There are two steps on this process, Installing and Configuration, below are the
steps I usually take when I used the scripts in this repo.

### Steps:
iredmail deployer instructions

# iRedmail Deployer
---
The goal of this repo is to automate the process of installing iRedmail with
the common setup/configuration we are doing when we are setting it up. 

There are two steps on this process, Installing and Configuration, below are the
steps I usually take when I used the scripts in this repo.

### Steps:
1. Determine what domain you want to use as an smtp.  Example: mysmtp.example.com

2. Create a new droplet on digitalocean where you want to install as an SMTP. Make sure it's Ubuntu 14.04 x64.

3. Go to DNSMadeEasy and assign the droplet IP to the domain (Create an A record).  Create a subdomain of one of the available domains, e.g., mysmtp.example.com

4. Ping the domain you pick in Step #1, and check the subdomain and your server's IP are linked.

5. Create a new 2nd droplet on digitalocean to serve as your "Deployer Server". ssh into this server.  

6. Clone this bitbucket repo on the server. 
`git clone https://wientjes@bitbucket.org/suposts/iredmail_deployer_hiring_test.git` 

7. Create ssh keys on this deployer droplet, using the following command: `ssh-keygen` this will prompt for a passphrase (twice), just hit enter when prompted. Once done this command is done it will create two files, a private key: `~/.ssh/id_rsa` and a public key: `~/.ssh/id_rsa.pub`

8. Append the public key on the `authorized_keys` files on the `iredmail_deployer_hiring_test/important_keys` directory using the following command: `cat ~/.ssh/id_rsa.pub >> iredmail_deployer_hiring_test/important_keys/authorized_keys` (This link shows you how to make ssh keys: https://www.digitalocean.com/community/tutorials/how-to-use-ssh-keys-with-digitalocean-droplets)

9. Goto the `iredmail_deployer_hiring_test` folder

10. SSH to your smtp domain you pick in Step #1 to make sure you can access it with the correct password.

11. When you're login run `apt-get update` command to update repositories

12. Exit the SSH session and go back to the `iredmail_deployer_hiring_test` folder in step #5, your deployer server.

13. Run the `install.sh` script with `./install.sh <smtp domain>` e.g. `./install.sh example.com`

14. Enter the password when required.

15. After the installation, your server/smtp will restart. Wait for it to be online and SSH again to the server. NOTE: You should not be required to enter a password anymore, if it requires you, there's something wrong with your installation.

16. When logged in, run the following command to check if these services are running: `service mysql status`   `service postfix status`   `service amavis status`  If all are running, then you're good to go on the configuration part of the setup.

17. Exit your server and go back to `iredmail_deployer_hiring_test` folder

18. Determine what domain you want to use for your sender e.g. mary@mysmtp.example.com then the sender domain is mysmtp.example.com. SMTP domain and sender domain is the same for this test.

19. Goto DNSMadeEasy and add the following text to the TXT records of the root of your sender domain (e.g. example.com). Name `dkim._domainkey.mysmtp`   Value `"v=DKIM1; p=""MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsy4dmfRXeReo1xMyKt6r""uJoOuTnZjeOkQs/iCc6d06RkAi59feEQScyHHFfgZIsBHIq9hmKQKjgDiMD7Djzk""61BBnumd0YOsfwMMDu9v6ml3jn1z5Kz1wv749KTaIaGKlR/V+xlr19ICHZBGj6sr""MRkaxNjbgF+7rlHUF4Xxltq4/Wd4lbs+gB+9Yp2MD5tgKC3RRUjV09jGk2AAi0Xy""XNI3Ag7OjJjLO8nMcpA5r19g/9vdXM5CTpz0VYM+tVkSUPAn+/Dh+kLah54o49gT""Sh4OC41PbWtBww3l9/UBoOPFl2xVKVGCMJ1do+rtAiXYfHwBC2cD7a9Jk0VjyLON""mQIDAQAB"` 

20. Add also the IP of your smtp server to the the SPF record for the sender domain, just add ip4:123.123.123.123, where the 123.123.123 is your IP address.

21. Run `./config.sh <smtp domain> <sender domain>` e.g. `./config.sh example.com example.com`

22. SSH again in your server and run `amavisd-new testkeys`, you should see one with "pass"

23. Run `tail -f /var/log/mail.log` to monitor your log file while you test your smtp with sendy.

24. Login to http://applicants.univepost.com and create your own brand(smtp) and input the details below for the credentials.

  Sendy Credentials App
    username: aj@berkelist.com
    password: applicants12345

  Sendy Credentials SMTP
    - host: yoursmtpdomain.com
    - username: mary@yoursmtpdomain.com
    - password: blastaway2016
    - port: 587
    - tls

Send yourself a sample email from sendy or any smtp client that you can add smtp credentials.

THAT'S IT, YOU HAVE NOW A FUNCTIONING SMTP READY!